{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "9a424b8a",
   "metadata": {},
   "source": [
    "# Neural Style Transfer\n",
    "\n",
    "\n",
    "### Table of contents:\n",
    "\n",
    "1. [Goals](#section_1)\n",
    "2. [Analysis Setup](#section_2)\n",
    "3. [Methodolodgy](#section_3)\n",
    "4. [Style Transfer Algorithm](#section_4)\n",
    "5. [Next Steps](#section_5)\n",
    "6. [Bibliography](#section_6)\n",
    "7. [Session Info](#section_7)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5a8bc661",
   "metadata": {},
   "source": [
    "## 1. Goals  <a class=\"anchor\" id=\"section_1\"></a>\n",
    "\n",
    "a. Explain intuition and methodolodgy for developing neural style transfer algorithm in a simple manner.\n",
    "\n",
    "\n",
    "b. Develop style transfer algorithm utilising Keras library with Tensor Flow backend and VGG19 model as a starting point."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "74830a0c",
   "metadata": {},
   "source": [
    "## 2. Analysis Setup <a class=\"anchor\" id=\"section_2\"></a>\n",
    "\n",
    "This notebook is aiming to showcase development of the neural style transfer algorithm so all custom functions will be showcased in logical order throughout <a class=\"anchor\" id=\"section_4\">section 4</a>. Furthermore, since development is intrinsically one-off, static in nature we include session information in <a class=\"anchor\" id=\"section_7\">section 7</a> in case anybody would like to replicate contents of this notebook in particular. For final operationalised version of the algorithm please use dependencies as specified in `environment/`."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "550fd2aa",
   "metadata": {},
   "source": [
    "### 2.1 Libraries"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "42ea0d79",
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Using TensorFlow backend.\n"
     ]
    }
   ],
   "source": [
    "# Tensor objects and mathematical operations\n",
    "import numpy as np\n",
    "# Image preprocessing functions\n",
    "from keras.preprocessing.image import load_img, img_to_array\n",
    "# Pretrained VGG19 model\n",
    "from keras.applications import vgg19\n",
    "# Keras backend utilities\n",
    "from keras import backend as kutils\n",
    "# Gradient ascent L-BFGS algorithm\n",
    "from scipy.optimize import fmin_l_bfgs_b\n",
    "# Utilities for reading and saving images\n",
    "from imageio import imread, imwrite\n",
    "# System time\n",
    "import time\n",
    "# Enchanced Python shell class for controlling Jupyter outputs\n",
    "from IPython.core.interactiveshell import InteractiveShell"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14613181",
   "metadata": {},
   "source": [
    "### 2.2 Custom Options"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "54f11b97",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Make Jupyter print all outputs\n",
    "InteractiveShell.ast_node_interactivity = \"all\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "015d8ebe",
   "metadata": {},
   "source": [
    "## 3. Methodolodgy <a class=\"anchor\" id=\"section_3\"></a>\n",
    "\n",
    "Our aim is to implement neural style transfer algorithm as introduced by [Leon Gatys et al. (2015) [1]](#source_1) and explain it well enough so you will be able to implement yourself. So full understanding is beyond scope but I will provide supplementary sources where relevant and provide intuition via code since it is much easier to comprehend than mathematical equations.\n",
    "\n",
    "The general idea is to create a model that takes a picture and changes its style while preserving its contents. Intuitively we can think of it as if we would give a picture to an artist and ask him to recreate it but in his own style. So here *style* equates to textures, colours and visual patterns; and the *content* is well... just that - if we would be talking about books instead of images it would be essentially the story e.g. imagine that Shakespear would rewrite Harry Potter - it would still be a story, aka content, about wizards but would use wildly different language (style).\n",
    "\n",
    "As the name suggests we will use neural network to achieve our end goal meaning that we will require a [loss function [2]](#source_2) that represent what we want to achieve it and minimise it. So if we would predicting house prices our loss function would look somethings like this:\n",
    "\n",
    "$Loss = TrueHousePrice - PredictedHousePrice$\n",
    "\n",
    "The challenge in this case that we are not interested in just a single thing but instead we want to preserve the content of input image and overlay the style of our style reference image. We can think of it as removing style of the original image, removing contents of the style reference image and adding the two together. Hence we want to minimise the following:\n",
    "\n",
    "$Loss = Distance(Style(StyleReferenceImage) - Style(GeneratedImage)) + Distance(Content(OriginalImage) - Content(GeneratedImage))$\n",
    "\n",
    "where $StyleReferenceImage$ is an image that we want to use as style reference, $OriginalImage$ is raw input image that we want to transform, $GeneratedImage$ is our output image, $Distance$ is some kind of [norm function [3]](#source_3) like L2 norm (commonly used for regularisation of ML models), $Content$ is a function that computes representation of its content given input image and $Style$ is a function that computes representation of style given input image.\n",
    "\n",
    "Since we are going to be minimising the above loss it means that $Style(GeneratedImage)$ will be very close to $Style(StyleReferenceImage)$ and $Content(GeneratedImage)$ will be very close to $Content(OriginalImage)$ thus achieving our goal.\n",
    "\n",
    "If you never worked with image data you might be wondering how we can perform any kind of mathematical operations with it, especially that ML models can only work with numerical data. Images they are nothing more than 3 dimensional* tensors (which are like multi-dimensional tables without headers) with values representing its height, width and channels. Since we are working with colour images the last one give us intensity of red, green and blue (commonly called [RGB [4]](#source_4)) for each pixel expressed in range between 0 and 255. Because of that we will need functions for transforming JPEG images into tensors and for transforming our output tensors back into images.\n",
    "\n",
    "Whilst you can intuitively understand what I mean by style and content we need to quantify those terms for our model. In [Convolutional Neural Networks [5]](#source_5) (which we will refer to as ConvNets from now on) activations from earlier layers contain local information whereas later layers contain increasingly global information. So we could broadly say that the former layers will contain style and the upper layers will be our source of content. Hence for content loss we will use L2 norm between the activations of an upper layer in a pretrained ConvNet computed over the target image and the generated image. Whilst for style loss we will use Gram matrix of multiple lower layers's activation - we can think of it as a map of the correlations (or measure of similarity) between the layer's features.\n",
    "\n",
    "If we would like to start from scratch we would build general purpose image recognition ConvNet but that would very costly so instead we will use [VGG19 [6]](#source_6) network.\n",
    "\n",
    "Therefore our high-level plan will be something like this:\n",
    "a) Use VGG19 to compute layer activations for style reference image, target image and generated image at the same time.\n",
    "b) Define loss function using all layer activations from a).\n",
    "c) Use gradient descent to minimise b).\n",
    "\n",
    "<sub>*I know, I know, technically it is more correct to call it a tensor of rank 3 but there is absolutely no need to call my math proffessor.</sub>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "08c4d353",
   "metadata": {},
   "source": [
    "## 4. Style Transfer Algorithm <a class=\"anchor\" id=\"section_4\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4a712716",
   "metadata": {},
   "source": [
    "## 5. Next Steps <a class=\"anchor\" id=\"section_5\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cff0e726",
   "metadata": {},
   "source": [
    "## 6. Bibliography <a class=\"anchor\" id=\"section_6\"></a>\n",
    "\n",
    "<a class=\"anchor\" id=\"source_1\">[1] Leon A. Gatys, Alexander S. Ecker and Matthias Bethge, \"A neural Algorithm of Artistic Style, arXiv (2015), https://arxiv.org/abs/1508.06576</a>\n",
    "\n",
    "<a class=\"anchor\" id=\"source_2\">[2] Jason Brownlee, \"Loss and Loss Functions for Training Deep Learning Neural Networks\", Machine Learning Mastery (2019), https://machinelearningmastery.com/loss-and-loss-functions-for-training-deep-learning-neural-networks/</a>\n",
    "\n",
    "<a class=\"anchor\" id=\"source_3\">[3] Jason Brownlee, \"Loss and Loss Functions for Training Deep Learning Neural Networks\", Machine Learning Mastery (2018), https://machinelearningmastery.com/vector-norms-machine-learning/</a>\n",
    "\n",
    "<a class=\"anchor\" id=\"source_4\">[4] Wikipedia, \"RGB color model\" (2022), https://en.wikipedia.org/wiki/RGB_color_model</a>\n",
    "\n",
    "<a class=\"anchor\" id=\"source_5\">[5] Fei-Fei Li, Ranjay Krishna and Danfei Xu, \"CS231n: Convolutional Neural Networks for Visual Recognition\" (2021), Stanford University, https://cs231n.github.io/convolutional-networks/</a>\n",
    "\n",
    "<a class=\"anchor\" id=\"source_6\">[6] Jianye Zhou, Xinyu Yang, Lin Zhang, Siyu Shao, Gangying Bian, \"Multisignal VGG19 Network with Transposed Convolution for Rotating Machinery Fault Diagnosis Based on Deep Transfer Learning\", Shock and Vibration, vol. 2020, Article ID 8863388, https://doi.org/10.1155/2020/8863388</a>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "918407a2",
   "metadata": {},
   "source": [
    "## 7. Session Info <a class=\"anchor\" id=\"section_7\"></a>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.11"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
